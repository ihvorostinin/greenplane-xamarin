﻿using System;

using Android.App;
using Android.Content.PM;
using Android.Views;
using Android.OS;
using Xamarin.Forms.Platform.Android;

namespace GreenPlane.Droid
{
	[Activity(Label = "GreenPlane.Droid", Icon = "@drawable/icon", Theme = "@style/MyTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
	public class MainActivity : FormsAppCompatActivity
	{
		protected override void OnCreate(Bundle bundle)
		{
			TabLayoutResource = Resource.Layout.Tabbar;
			ToolbarResource = Resource.Layout.Toolbar;

			base.OnCreate(bundle);

			global::IXGeeks.Forms.Forms.Init(this, bundle);
			global::Xamarin.Forms.Forms.Init(this, bundle);
			global::Xamarin.FormsMaps.Init(this, bundle);

			LoadApplication(new App());
		}

		public override bool OnCreateOptionsMenu(IMenu menu)
		{
			var item = menu.Add("test1");

			return base.OnCreateOptionsMenu(menu);
		}
	}
}
