﻿using System;
using Xamarin.Forms;

namespace GreenPlane.Metrics
{
	public abstract class ModelInfoConverter : IValueConverter
	{

		protected abstract int Index { get; }
		protected abstract string GetValue(Tuple<string, string> x);

		public object Convert(object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			var model = value as ModelInfo;

			if (model.Count > Index)
				return GetValue(model[Index]);
			else
				return string.Empty;
		}

		public object ConvertBack(object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			throw new System.NotImplementedException();
		}

	}

	public class ModelInfoConverter01 : ModelInfoConverter
	{
		protected override int Index { get { return 0; } }
		protected override string GetValue(Tuple<string, string> x) { return x.Item1; }
	}

	public class ModelInfoConverter02 : ModelInfoConverter
	{
		protected override int Index { get { return 0; } }
		protected override string GetValue(Tuple<string, string> x) { return x.Item2; }
	}

	public class ModelInfoConverter11 : ModelInfoConverter
	{
		protected override int Index { get { return 1; } }
		protected override string GetValue(Tuple<string, string> x) { return x.Item1; }
	}

	public class ModelInfoConverter12 : ModelInfoConverter
	{
		protected override int Index { get { return 1; } }
		protected override string GetValue(Tuple<string, string> x) { return x.Item2; }
	}

	public class ModelInfoConverter21 : ModelInfoConverter
	{
		protected override int Index { get { return 2; } }
		protected override string GetValue(Tuple<string, string> x) { return x.Item1; }
	}

	public class ModelInfoConverter22 : ModelInfoConverter
	{
		protected override int Index { get { return 2; } }
		protected override string GetValue(Tuple<string, string> x) { return x.Item2; }
	}
}
