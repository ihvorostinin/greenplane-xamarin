﻿using System;
using System.Collections.Generic;
using DLToolkit.Forms.Controls;
using GreenPlane.Metrics;
using Xamarin.Forms;

namespace GreenPlane.Metrics
{
	public partial class View : ContentView, IView
	{

		FlowListView ListView { get { return listView; } }

		public View()
		{
			InitializeComponent();
		}

		// -------------------------------------------------------------------------------------------------------------
		// IMetricsPage
		// -------------------------------------------------------------------------------------------------------------

		void IView.Show(List<Model> data)
		{
			ListView.FlowItemsSource = data;
		}

		void IView.onReorderedAfter(Reorder callback)
		{
		}

		void IView.onReorderedBefore(Reorder callback)
		{
		}

		// -------------------------------------------------------------------------------------------------------------
		// -
		// -------------------------------------------------------------------------------------------------------------
	}
}
