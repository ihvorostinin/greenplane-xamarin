﻿using System;
using System.Collections.Generic;

namespace GreenPlane.Metrics
{

	public delegate void Reorder();

	public interface IView
	{

		void Show(List<Model> data);
		void onReorderedAfter(Reorder callback);
		void onReorderedBefore(Reorder callback);

	}
}
