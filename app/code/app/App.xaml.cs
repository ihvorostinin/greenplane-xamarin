﻿using System.Diagnostics;
using Xamarin.Forms;

namespace GreenPlane
{
	public partial class App : Application
	{
		Main.Controller MainController { get; }

		public App()
		{
			InitializeComponent();

			var mainPage = new Main.Page();

			MainPage = mainPage;
			MainController = new Main.Controller(mainPage);

			MainController.Load();
			MainController.Show();
		}

		protected override void OnStart()
		{
			// Handle when your app starts
		}

		protected override void OnSleep()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume()
		{
			// Handle when your app resumes
		}
	}
}
