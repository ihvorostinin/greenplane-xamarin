﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Xamarin.Forms;

namespace GreenPlane.Metrics
{
	public class Controller
	{

		IView View { get; }
		List<Model> Data { get; }

		public Controller(IView view)
		{
			View = view;
			Data = new List<Model>();

			// init data stub
			// !!! ids should start from 0
			// !!! ids increment should be 1
			// if remove/add you should migrate DB

			ModelInfo info = new ModelInfo();
			Func<Stream> icon = null;

			info.Add(Tuple.Create("Speed", "51 MPH"));
			info.Add(Tuple.Create("Altitude", "45 Ft"));
			info.Add(Tuple.Create("Heading", "360.0"));
			//icon = () =>
			//{
			//	return Assembly..GetManifestResourceStream("");
			//};
			//metric!.icon = UIImage(named: "MetricsIconPlaceholder")

			Data.Add(new Model(0, icon, info));

			for (var i = 1; i < 50; i++)
			{
				info = new ModelInfo();

				info.Add(Tuple.Create("Name", "Val" + i));
				//metric!.icon = UIImage(named: "MetricsIconPlaceholder")

				Data.Add(new Model(i, icon, info));
			}
		}

		////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		#region Actions
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		public void Load()
		{
		}
		
		public void Show()
		{
			View.Show(Data);
		}

		////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		#endregion
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	}
}
