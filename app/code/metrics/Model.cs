﻿using System;
using System.Collections.Generic;
using System.IO;
using Xamarin.Forms;

namespace GreenPlane.Metrics
{

	public class ModelInfo : List<Tuple<string, string>> { }

	public class Model
	{
	
		public string ID { get; } // initial order num (unique)
		public Func<Stream> Icon { get; }
		public ModelInfo Info { get; }
		public List<string> Test { get; } = new List<string>();

		public Model(int id, Func<Stream> icon, ModelInfo info)
		{
			ID = id.ToString();
			Icon = icon;
			Info = info;
			Test.Add("asd1");
		}

	}
}
