﻿using System;

namespace GreenPlane.Main
{
	public class Controller
	{
		public Metrics.Controller Metrics { get; }

		public Controller(Page view)
		{
			Metrics = new Metrics.Controller(view.Metrics);
		}

		public void Load()
		{
			Metrics.Load();
		}

		public void Show()
		{
			Metrics.Show();
		}

	}
}
