﻿using Xamarin.Forms;

namespace GreenPlane.Main
{
    public partial class Page : MasterDetailPage
    {
		public Metrics.View Metrics { get { return metrics; } }
		public Status.View Status { get { return status; } }
		public Map.View Map { get { return map; } }
		public Chat.View Chat { get { return chat; } }

        public Page()
        {
			InitializeComponent();
        }
    }
}
